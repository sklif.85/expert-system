class InferenceEngine:

    '''Class InferenceEngine contains attributes:
    known_facts - list of facts id that known to be true;
    known_values - list facts values(None, True, False) in order they appear in knowledge base;
    goals - list of facts id, that needs to be proven;
    base - actual knowledge base.
    Methods include backward chaining'''

    def __init__(self, known_facts, known_values, goals, knowledge_base):
        self.known_facts = known_facts
        self.known_values = known_values
        self.goals = goals
        self.base = knowledge_base

    def __repr__(self):
        return "%s(%r,\n %r,)" % (self.__class__, self.known_facts,
                                       self.goals)

    def __str__(self):
        a = " Class KnowledgeBase\n" + "Known Facts list of id: \n" \
            + str(self.known_facts) \
            + "\nGoal Facts list: \n" + str(self.goals)
        return a

    def backward_chaining(self):
        for goal in self.goals:
            self.go_backward(goal)

    def go_backward(self, goal):
        if goal not in self.known_facts:
            rules = self.base.get_all_rules(goal)
            if not rules:
                self.known_facts.append(goal)
                self.known_values[goal] = False
            for rule in rules:
                conditions = self.base.get_conditions(rule)
                for fact in conditions:
                    self.go_backward(fact)
                self.solve_rule(rule)

    def solve_rule(self, rule_id):
        rule = self.base.get_rule(rule_id)
        str_list = [str(self.known_values[item]) if item in self.known_facts
                    else item for item in rule.condition]
        # print(str_list)
        result = eval(" ".join(str_list))
        # print(result)
        for item in rule.conclusion:
            # if self.known_facts[rule.conclusion] is not None:
            # check for consistency
            self.known_facts.append(item)
            self.known_values[item] = result

        # print("Fire rule #", rule_id)
        # print(self.base.get_rule(rule_id))

    def print_result(self):
        # print(self.goals) #tst
        for goal in self.goals:
            print("Fact \"{}\" is {}".format(self.base.get_fact_text(goal),
                                             self.known_values[goal]))