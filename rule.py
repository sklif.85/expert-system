class Rule:

    '''Class Rule has atributes:
    condition - "if" part of the rule,
    contains list with operators and operands(fact id);
    conclusion - "then" part of the rule,
    contains list with operators and operands(fact id);'''

    def __init__(self, condition = None, conclusion = None):
        self.condition = condition
        self.conclusion = conclusion

    def __repr__(self):
        return "%s(%r, %r)" % (self.__class__, self.condition,
                                       self.conclusion)

    def __str__(self):
        a = "Сlass Rule\n" + "Condition: \n" + str(self.condition) \
            + "\nConclusion: \n" + str(self.conclusion)
        return a
