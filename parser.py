import sys
import argparse
import os
import shlex
import re
import rule as rl
import knowledge_base as kb
import inferense_engine as ie

operators = ['+', '^', '->', '|', '(', ')', '!', '?', '=']

# rule_regex =
#doesn't support braces in conclusion, bad "!(" managment
rule_regex_raw = r'^\(?\!?(?:\"[^\"]*\"\s?)' \
                 r'([+|^]\s*\(*\!?(?:\"[^\"]*\"\)*\s*))' \
                 r'*\-\>\s*\!?(?:\"[^\"]*\"\s*)' \
                 r'(\+\s*\!?(?:\"[^\"]*\"\s*))*$'

# initial_regex = r'^=\s*([A-Z]\s*)*\s*$'
# initial_regex_raw = r'^=\s*(?:\"[^\"]*\"\s*)*$'

# query_regex = r'^\?\s*([A-Z]\s*)*$'
# query_regex_raw = r'^\?\s*(?:\"[^\"]*\"\s*)*$'

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    parser.add_argument('-r', "--raw", help='allow facts as raw text',
                        action="store_true")
    return parser.parse_args()

def parse_letters(list):
    new = [re.findall('([A-Z])', x) if re.findall('([A-Z])', x) else x for x in list]
    new = [item for sublist in new for item in sublist]
    return new

def validate_file(lines, raw):
    """
    Validate a copy of lines, converted to string for regex use

    :param lines: list of lines with parsed facts and operators as elements
                    (except 2 last lines for raw == False)
    :param raw: flag raw boolean
    :return: lines or lines with parsed 2 last lists (see else) if everything valid
            else raise exception and catch it in previous function (read_file)
    """

    if raw:
        pass
    #validate raw
    else:
    # in initial ("= [A-Z]") and query ("? [A-Z]") lines
    # letters should be parsed as separate elements of list
    # after validation
        lines[-2] = parse_letters(lines[-2])
        lines[-1] = parse_letters(lines[-1])

    return lines

def read_file(filename, raw=False):
    try:
        if os.path.getsize(filename) > 0:
            with open(filename, 'r') as file:
                text = file.readlines()
                lines = [list(shlex.shlex(item, punctuation_chars
                         ='+, ^, ->, |, (, ), =, ?')) for item in text]
                if not lines:
                    exit(1)
                lines = [item for item in lines if item]
                lines = validate_file(lines, raw)
                # parse goals and facts A-Z lines
                return lines
        else:
            raise EOFError
    except EOFError:
        print("Empty file")
    except FileNotFoundError:
        print("File not found")
    except IsADirectoryError:
        print("Is a directory")
    except PermissionError:
        print("Don't have permission to read a file")
    except IOError:
        print("Fail to read the file")
    except AttributeError:
        print("Probably some shit in parsing department")


def fill_engine(lines, base):
    known_facts = []
    goals = []
    for line in lines:
        if line[0] == '=':
            known_facts.extend(line[1:])
        elif line[0] == '?':
            goals.extend(line[1:])
    known_facts = [base.get_fact_id(item) for item in known_facts
                   if base.get_fact_id(item) is not None]
    if not known_facts:
        raise ValueError
    goals = [base.get_fact_id(item) for item in goals
             if base.get_fact_id(item) is not None]
    if not goals:
        raise ValueError
    # print(goals) #tst
    # print(known_facts) #tst

    known_values = [True if id in known_facts
              else None
              for id, item in enumerate(range(base.get_facts_len()))]
    return ie.InferenceEngine(known_facts, known_values, goals, base)


def fill_knowledge_base(lines):
    facts = [item for i in lines for item in i if item not in operators]
    # print(facts) #tst
    facts = list(set(facts))
    rules = []
    grid = [[] for i in range(len(lines))]
    for id, item in enumerate(lines):
        rule = [facts.index(j) if j not in operators
                else 'and' if j == '+'
                else 'not' if j == '!'
                else j for j in item]
        idx = rule.index('->')
        rules.append(rl.Rule(rule[:idx], rule[(idx + 1):]))
        grid[id] = [1 if x in rules[id].condition
                    else 2 if x in rules[id].conclusion
                    else 0 for x,y in enumerate(range(len(facts)))]
    return kb.KnowledgeBase(grid, facts, rules)


def parse_input():
    args = parse_arguments()
    # print(args.raw) #tst
    lines = read_file(args.filename, args.raw)
    if not lines:
        sys.exit()
    # print(lines) #tst
    base = fill_knowledge_base(lines[:-2])
    # print(base) #tst
    try:
        engine = fill_engine(lines[-2:], base)
    except ValueError:
        print("Facts you are trying to initialise or query "
              "are unknown to knowledge base")
        sys.exit()
    return engine

# parse_input()
# print(args)

def main():
    engine = parse_input()

    engine.backward_chaining()
    engine.print_result()

if __name__ == "__main__":
    main()