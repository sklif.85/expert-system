import numpy as np

class KnowledgeBase:

    '''Class knowledge base contains:
    ternary grid with rules and all facts connections,
    rules list and facts list'''

    def __init__(self, ternary_grid=None, facts=None, rules=None):
        self.__grid = ternary_grid
        self.___facts = facts
        self.___rules = rules

    def __repr__(self):
        return "%s(%r,\n %r,\n %r)" % (self.__class__, self.__grid,
                                       self.___facts, self.___rules)

    def __str__(self):
        a = " Class KnowledgeBase\n" + "Ternary grid: \n" + str(self.__grid) \
            + "\nFacts list: \n" + str(self.___facts) + "\nRules list: \n" \
            + str(self.___rules)
        return a

    def get_fact_id(self, item):
        if item in self.___facts:
            return self.___facts.index(item)
        else:
            return None

    def get_facts_len(self):
        return len(self.___facts)

    def get_fact_text(self, id):
        return(self.___facts[id])

    def get_rule(self, rule_num):
        return(self.___rules[rule_num])

    def get_all_rules(self, goal):
        rules = [idx for idx, item in enumerate(self.__grid) if item[goal] == 2]
        return rules

    def get_conditions(self, rule):
        conditions = [idx for idx, item in enumerate(self.__grid[rule]) if item == 1]
        return conditions